from uuid import UUID

from pydantic import BaseModel


class SharedLink(BaseModel):
    shared_uuid: str


class SharedLinkInResponse(SharedLink):
    shared_gallery_link_template: str = "../api/galleries/shared/\{shared_uuid\}"
