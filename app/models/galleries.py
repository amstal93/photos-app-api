from decimal import Decimal
from typing import Dict, List, Optional, Union
from uuid import UUID

from app.models.mixins import DateTimeModelMixin, PositionedItemMixin
from app.models.photos import (Photo, PhotoInDB, PhotoListInReorderRequest,
                               PositionedPhoto)
from app.utils import security
from pydantic import BaseModel, EmailStr, HttpUrl


class Gallery(BaseModel):
    name: str


class GalleryWithId(Gallery):
    gallery_id: int


class GalleryWithIdInResponse(GalleryWithId):
    gallery_link_template: str = "../api/galleries/\{gallery_id\}"


class GalleryInPatchRequest(Gallery):
    photos_to_attach: Optional[List[Photo]] = []
    photos_to_delete: Optional[List[Photo]] = []
    photos_to_reorder: Optional[PhotoListInReorderRequest] = []


class GalleryList(BaseModel):
    galleries: List[Optional[GalleryWithId]] = []


class GalleryListInResponse(GalleryList):
    gallery_link_template: str = "../api/galleries/\{gallery_id\}"


class GalleryListInReorderRequest(BaseModel):
    new_index: int
    galleries: List[int]


class PositionedGallery(GalleryWithId, PositionedItemMixin):
    pass


class GalleryListWithPositions(BaseModel):
    galleries: List[Optional[PositionedGallery]] = []


class GalleryInDB(GalleryWithId, DateTimeModelMixin):
    shared_uuid: Optional[str]


class PhotoWithGalleriesInResponse(PhotoInDB, GalleryListInResponse):
    # prevent circular import
    pass
