import pathlib

import aiosql

# https://nackjicholson.github.io/aiosql/defining-sql-queries.html

queries = aiosql.from_path(pathlib.Path(__file__).parent / "sql", "asyncpg")
