-- name: get-user-by-email^
SELECT id,
    email,
    salt,
    passhash,
    created_at
FROM users
WHERE email = :email
LIMIT 1;
-- name: create-new-user<!
INSERT INTO users (email, salt, passhash)
VALUES (:email, :salt, :passhash)
RETURNING id,
    created_at;