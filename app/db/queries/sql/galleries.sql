-- name: get-galleries-by-user-id
SELECT gallery_id,
    name,
    position
FROM galleries_accesses ga
    JOIN galleries g ON g.id = ga.gallery_id
WHERE user_id = :user_id
ORDER BY position;
-- name: get-gallery-by-name^
SELECT g.id
FROM galleries_accesses ga
    JOIN galleries g ON g.id = ga.gallery_id
WHERE name = :name
    AND user_id = :user_id;
-- name: create-new-gallery<!
INSERT INTO galleries (name)
VALUES (:name)
RETURNING id,
    created_at;
-- name: create-new-gallery-access!
INSERT INTO galleries_accesses (gallery_id, user_id, is_owned)
VALUES (:gallery_id, :user_id, :is_owned);
-- name: get-gallery-by-gallery-id^
SELECT name,
    shared_uuid,
    created_at
FROM galleries_accesses ga
    JOIN galleries g ON g.id = ga.gallery_id
WHERE gallery_id = :gallery_id
    AND user_id = :user_id;
-- name: change-galleries-position*!
UPDATE galleries_accesses
SET position = :position
WHERE gallery_id = :gallery_id;
-- name: update-gallery-name!
UPDATE galleries
SET name = :name
WHERE gallery_id = :gallery_id;
-- name: remove-photos-from-gallery*!
DELETE FROM photos_attachments
WHERE photo_uuid = :photo_uuid
    AND gallery_id = :gallery_id
RETURNING *;
-- name: attach-photos-to-gallery*!
INSERT INTO photos_attachments (gallery_id, photo_uuid)
VALUES (:gallery_id, :photo_uuid);
-- name: delete-gallery!
DELETE FROM galleries
WHERE id = :gallery_id;
-- name: update-shared-uuid!
UPDATE galleries
SET shared_uuid = :shared_uuid
WHERE id = :gallery_id;
-- name: remove-shared-uuid!
UPDATE galleries
SET shared_uuid = NULL
WHERE id = :gallery_id;
-- name: get-gallery-by-shared-uuid^
SELECT name,
    gallery_id,
    created_at
FROM galleries_accesses ga
    JOIN galleries g ON g.id = ga.gallery_id
WHERE gallery_id = :gallery_id;
-- name: get-unattached-gallery-id$
SELECT gallery_id
FROM galleries g
WHERE name = 'unattached'
    AND user_id = :user_id;