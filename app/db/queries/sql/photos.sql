-- name: get-photos-by-user-id
SELECT photo_uuid
FROM photos_attachments pa
    JOIN galleries_accesses ga ON pa.gallery_id = ga.gallery_id
WHERE user_id = :user_id;
-- name: get-photos-by-gallery-id
SELECT photo_uuid,
    pa.position
FROM photos_attachments pa
    JOIN galleries_accesses ga ON pa.gallery_id = ga.gallery_id
WHERE user_id = :user_id
    AND ga.gallery_id = :gallery_id
ORDER BY pa.position;
-- name: get-unattached-photos-by-user-id
SELECT photo_uuid,
    pa.position
FROM photos_attachments pa
    JOIN galleries_accesses ga ON pa.gallery_id = ga.gallery_id
    JOIN galleries g ON ga.gallery_id = g.id
WHERE user_id = :user_id
    AND g.name = 'unattached'
ORDER BY pa.position;
-- name: change-photos-position*!
UPDATE photos_attachments
SET position = :position
WHERE photo_uuid = :photo_uuid
    AND gallery_id = :gallery_id;
-- name: get-photos-by-photo-uuid^
SELECT photo_uuid,
    metadata,
    created_at
FROM photos p
    JOIN photos_attachments pa ON p.uuid = pa.photo_uuid
    JOIN galleries_accesses ga ON pa.gallery_id = ga.gallery_id
WHERE user_id = :user_id
    AND photo_uuid = :photo_uuid;
-- name: add-photos*!
INSERT INTO photos (photo_uuid, metadata)
VALUES (:photo_uuid, :metadata);